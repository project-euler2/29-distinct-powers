import math
import decimal
import time

start_time = time.time()

def distinct_power(a,b):
    distinct_powers = []
    for i in range(2,a+2):
        for j in range(2,b+2):
            distinct_powers.append(decimal.Decimal(i*math.log2(j)))
    return len(list(dict.fromkeys(distinct_powers)))

print(distinct_power(99,99))
print(f"--- {(time.time() - start_time):.10f} seconds ---"  )